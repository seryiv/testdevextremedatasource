import { Component } from '@angular/core';
import ODataContext from 'devextreme/data/odata/context';
import DataSource from 'devextreme/data/data_source';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'TestDevextremeDataSource';

  context: any;
  dataSource: DataSource;

  entities = {
    Users: {
      key: 'Id',
      keyType: 'String'
    }
  };

  selectVisible = true;

  constructor() {
    this.context = new ODataContext({
      url: `http://localhost:53320/api/agro/odata`, // replace with your url with a lot of o data records (at least several pages)
      version: 4,
      errorHandler: (error) => {
        console.log(error);
      },
      entities: this.entities
    });
    this.dataSource = new DataSource(this.context.Users);
  }

  btnClick(e) {
    this.selectVisible = !this.selectVisible;
  }

}
